import copy
import math
import sys

f = open('testInput_11.txt', 'r')
f1 = open('next_state.txt', 'w')
f2 = open('traverse_log.txt', 'w') 

#f  = open(sys.argv[2],'r')

f1.write("")
task = f.readline().strip('\n');
task = int(task)
#print "Task" + str(task)
player = f.readline().strip('\n');
player = int(player)
#print "Player" + str(player)
cutoff_depth = f.readline().strip('\n');
cutoff_depth = int(cutoff_depth)
#print "cutoff_depth" + str(cutoff_depth)
board_state2 = f.readline().strip('\n').strip('\r');
board_state2 = board_state2.split(' ')
#print "board_state2" + str(board_state2)
board_state1 = f.readline().strip('\n').strip('\r');
board_state1 = board_state1.split(' ')
#print "board_state1" + str(board_state1)
rows = 2
columns = len(board_state1)
#print "columns" + str(columns)
mancala2 = f.readline().strip('\n');
mancala1 = f.readline().strip('\n');
mancala1 = int(mancala1)
mancala2 = int(mancala2)
mancala = [0, mancala1, mancala2]
#print "mancala" + str(mancala)

if player == 1:
	maximizingPlayer = 1
	playerArray = board_state1
	opponentArray = board_state2
else:
	maximizingPlayer = 2
	playerArray = board_state2
	opponentArray = board_state1	

class State:
    def __init__(self, player, pit, playerArray, opponentArray, mancala, depth):
    	self.player = player
    	self.pit = pit
    	self.playerArray = playerArray
    	self.opponentArray = opponentArray
    	self.mancala = mancala
    	self.depth = depth
    	
def move1 (state, player, pit):    #move for player 1
	f2.write(pit)
	f2.write(" " + str(state.depth))
	f2.write("\n")
	#print "-------------------------------pit" + str(pit)
	#print "opponentArray" + str(state.opponentArray)
	#print "playerArray" + str(state.playerArray)
	#print "mancala" +str(state.mancala)
	
	if player == state.player:
		playerArray = copy.deepcopy(state.playerArray)
		opponentArray = copy.deepcopy(state.opponentArray)
	else:
		playerArray = copy.deepcopy(state.opponentArray)
		opponentArray = copy.deepcopy(state.playerArray)
	pos = int(pit[1]) - 2 
	stones = playerArray[pos]
	#print stones
	stones = int(stones)	
	if stones == 0:
		return state	
	mancala = copy.deepcopy(state.mancala)
	depth = copy.deepcopy(state.depth)
	depth = int(depth) + 1             #this state is the child
	#print playerArray
	#print opponentArray
	playerArray[pos] = 0
	position = pos + 1
	#print "pos" + str(pos)
	#print position
	last = ""                       #pit where the last stone is put, this to check for capture and extra chance
	while stones > 0:
		for i in range(position, columns):
			if(stones > 0):
				#print i
				playerArray[i] = int(playerArray[i]) + 1
				stones -= 1
				continue
			else:
				#print "here"
				#print i
				last = "b"+ str(i-1)
				break
		#print "stones" + str(stones)
		#print "opponentArray" + str(opponentArray)
		#print "playerArray" + str(playerArray)
		if stones == 0 and last == "":
			last = "b" + str(columns-1)
			break
		
		if stones > 0:
			mancala[player] += 1
			stones -= 1
		#print "stones" + str(stones)	
		#print "mancala updated"
		if stones == 0 and last == "":
			last = "b" + str(columns+2)
			break
		#print "going to opponent"
		for i in reversed(xrange(columns)):	
			#print stones
			if(stones > 0):
				opponentArray[i] = int(opponentArray[i]) + 1
				stones -= 1
				continue
			else:
				break
		position = 0
	#print "updated opponent"	
	#print "opponentArray" + str(opponentArray)
	#print "playerArray" + str(playerArray)
	#print "mancala" +str(mancala)	
	if last == "":
		state = State(player, pit, playerArray, opponentArray, mancala, depth)
		#print "opponentArray------------------------------" + str(opponentArray)
		#print "playerArray--------------------------------" + str(playerArray)
		#print "mancala------------------------------------" + str(mancala)
		#print"I'm returning now"
		return state	
	#print "last" + str(last)	
	#print "b" + str(columns+1)
	if last == "b" + str(columns+2):
		#print "opponentArray" + str(opponentArray)
		#print "playerArray" + str(playerArray)
		#print "Extra move as landed in mancala"
		state1 = State(player, pit, playerArray, opponentArray, mancala, depth-1)
		state1 = extraMove(state1, state1.player)
		return state1
	elif last[0] == "b" and playerArray[int(last[1])] == 1:
		playerArray[int(last[1])] = 0
		capture = int(opponentArray[int(last[1])]) + 1
		opponentArray[int(last[1])] = 0	
		mancala[player] = mancala[player] + capture
		
	state = State(player, pit, playerArray, opponentArray, mancala, depth)
	
	if checkTerminalState(state, player) == True:
		stones = 0
		for i in range(0, columns):
			state.opponentArray[i] = int(state.opponentArray[i])
			stones += int(state.opponentArray[i])
 			state.opponentArray[i] = 0
		state.mancala[2] += stones
		return state
	
	
	#print "playerArray---------------------------" + str(playerArray)
	#print "opponentArray-------------------------" + str(opponentArray)
	#print "mancala---------------------------------" + str(mancala)	
	return state
	
def move2 (state, player, pit):
	f2.write(pit)
	f2.write(" " + str(state.depth))
	f2.write("\n")
	#print "-----------------------------------------pit" + str(pit)
	#print "opponentArray" + str(state.opponentArray)
	#print "playerArray" + str(state.playerArray)
	#print "mancala" +str(state.mancala)
	#print "player" + str(player)
	#print "state player" + str(state.player)
	if player == state.player:
		playerArray = copy.deepcopy(state.playerArray)
		opponentArray = copy.deepcopy(state.opponentArray)
	else:
		playerArray = copy.deepcopy(state.opponentArray)
		opponentArray = copy.deepcopy(state.playerArray)
	
	#print "opponentArray" + str(opponentArray)
	#print "playerArray" + str(playerArray)	
	position = int(pit[1]) - 2 
	stones = playerArray[position]
	stones = int(stones)
		
	if stones == 0:
		return state
		
	mancala = copy.deepcopy(state.mancala)
	depth = copy.deepcopy(state.depth)
	depth = int(depth) + 1
	playerArray[position] = 0
	last = ""
	#print "opponentArray" + str(opponentArray)
	#print "playerArray" + str(playerArray)
	#print "mancala" +str(mancala)
	while stones > 0:
		for i in reversed(xrange(position)):	
			if(stones > 0):
				playerArray[i] = int(playerArray[i]) + 1
				#print i
				stones -= 1
				continue
			else:
				#print "here2"
				#print i
				last = "a"+ str(i+3)
				#print last
				break
		#print "stones" + str(stones)
		#print "opponentArray" + str(opponentArray)
		#print "playerArray" + str(playerArray)
		#print "mancala" +str(mancala)
		
		if stones == 0 and last == "":
			#print "here3"
			last = "a2"
			break
		if stones > 0:
			mancala[player] +=  1
			stones -= 1
		#print "stones" + str(stones)
		#print "updated mancala"
		#print "stones" + str(stones)
		#print "opponentArray" + str(opponentArray)
		#print "playerArray" + str(playerArray)
		#print "mancala" +str(mancala)
		
		if stones == 0 and last == "":
			#print "here2"
			last = "a1"
			break
		
		#print "stones" + str(stones)
		#print "going to opponent"	
		for i in range(0, columns):
			if(stones > 0):
				#print i
				opponentArray[i] = int(opponentArray[i]) + 1
				stones -= 1
				continue
			else:
				break
		position = columns
	#print "opponentArray" + str(opponentArray)
	#print "playerArray" + str(playerArray)
	#print "mancala" +str(mancala)		
	
	if last == "":
		state1 = State(player, pit, playerArray, opponentArray, mancala, depth)
		return state1	
	#print "last" + str(last)
	if last == "a1":
		#print "stone landed in Mancala"
		state1 = State(player, pit, playerArray, opponentArray, mancala, depth-1)
		#print "state 1 opponentArray" + str(state1.opponentArray)
		#print "state 1 playerArray" + str(state1.playerArray)
		#print "state 1 mancala" +str(state1.mancala)
		#print "going in extra move"
		state1 = extraMove(state1, state1.player)
		#print "playerArray---------------------------" + str(playerArray)
		#print "opponentArray-------------------------" + str(opponentArray)
		#print "mancala---------------------------------" + str(mancala)	
		return state1	
	elif last[0] == "a" and playerArray[int(last[1]) - 2] == 1:
		playerArray[int(last[1]) - 2] = 0
		capture = int(opponentArray[int(last[1]) - 2]) + 1
		opponentArray[int(last[1]) - 2] = 0	
		mancala[player] = mancala[player] + capture

	#print "playerArray---------------------------" + str(playerArray)
	#print "opponentArray-------------------------" + str(opponentArray)
	#print "mancala---------------------------------" + str(mancala)	
	state1 = State(player, pit, playerArray, opponentArray, mancala, depth)
	
	if checkTerminalState(state, player) == True:
		stones = 0
		for i in range(0, columns):
			state.opponentArray[i] = int(state.opponentArray[i])
			stones += int(state.opponentArray[i])
 			state.opponentArray[i] = 0
		state.mancala[1] += stones
		return state
		
	return state1
	
def evaluationFunction (state, player):
	if player == 1:
		opponent = 2
	else:
		opponent = 1	
	value = state.mancala[player] - state.mancala[opponent]	
	#print "eval" + str(value)
	return value
	
def checkTerminalState(state, player):
	terminalState = False
	if player == state.player:
		playerArray = copy.deepcopy(state.playerArray)
	else:
		playerArray = copy.deepcopy(state.opponentArray)
	count = 0
	if player == 1:
		for i in range(0, columns):
			if playerArray[i] ==0:
				count += 1
		if count == columns:
			return True
		else:
			return False
	if player == 2:
		for i in reversed(xrange(columns)):
			if playerArray[i] ==0:
				count += 1
		if count == columns:
			return True
		else:
			return False	
			
def findMax(value):
	#print "in max"
	value.sort(key=lambda x: x[1])
	#print "value" + str(value)
	max = value[len(value) - 1] 
	#print "max" + str(max)
	max_values = []
	for i in value:
		if i[1] == max[1]:
			max_values.append(i)
	#print "max values" + str(max_values)	
	max_values.sort(key=lambda x: x[0])
	#print "max values" + str(max_values)
	max = max_values[0]
	#print max
	max = max[0]	
	return max
	
def findMin(value):
	value.sort(key=lambda x: x[1], reverse=True)
	#print "value" + str(value)
	min = value[len(value) - 1] 
	#print "min" + str(min)
	min_values = []
	for i in value:
		if i[1] == min[1]:
			min_values.append(i)
			
	min_values.sort(key=lambda x: x[0])
	min = min_values[0]
	min = min[0]	
	return min	
	
def extraMove(state, player):
	if player == 1:
		row = "b"
		opponent = 2
	else:
		row = "a"
		opponent = 2
	value1 = []
	#print "in extra move"
	for i in range(0, columns):
		if player == 1:
			f2.write(row + str(i+2))
			f2.write(" " + str(state.depth))
			f2.write("\n")
			state1 = move1(state, player, row + str(i+2))
		else:
			#print "going to move 2"
			#print "state opponentArray" + str(state.opponentArray)
			#print "state playerArray" + str(state.playerArray)
			#print "state mancala" +str(state.mancala)
			f2.write(row + str(i+2))
			f2.write(" " + str(state.depth))
			f2.write("\n")
			
			state1 = move2(state, player, row + str(i+2))	
		value1.append([row + str(i+2), evaluationFunction(state1, player)])	
	#print "here"
	if player == maximizingPlayer:
		val = findMax(value1)
	else:
		val = findMin(value1)	
	if player == 1:	
		state1 = move1(state, player, val)
	else: 	
		state1 = move2(state, player, val)
	return state1
									

def greedy (state, player):
	if player == 1:
		row = "b"
		opponent = 2
	else:
		row = "a"
		opponent = 2
		
	value = []
	for i in range(0, columns):
		if player == 1:
			state1 = move1(state, player, row + str(i+2))
		else:
			state1 = move2(state, player, row + str(i+2))	
		value.append([row + str(i+2), evaluationFunction (state1, player)])
		#print "here1"
		#print value
		max = findMax(value)
	if player == 1:	
		final_state = move1(state, player, max)
	else: 	
		final_state = move2(state, player, max)
	if checkTerminalState(final_state, player) == True:
		stones = 0
		for i in range(0, columns):
			final_state.opponentArray[i] = int(final_state.opponentArray[i])
			stones += final_state.opponentArray[i]
 			final_state.opponentArray[i] = 0
		final_state.mancala[opponent] += stones
		output_board(final_state)
		return
	else:
		output_board(final_state)
		return

def minimax1 (state, player, cutoff_depth):
	#print "in minimax"
	row = "b"
	opponent = 2
	opponent_row = "a"
	while cutoff_depth >= 0:
		cutoff_depth -= 1
		value = []
		for i in range(0, columns):
			if player == 1:
				state1 = move1(state, player, row + str(i+2))
			elif player == 2:
				state1 = move2(state, player, row + str(i+2))
			if cutoff_depth >= 0:
				value2 = []			
				for j in range(0, columns):
					if cutoff_depth >= 0:
						if state1.player == 1:	
							state2 = move2(state1, opponent, opponent_row + str(j+2))
						elif state1.player == 2:
							state2 = move1(state1, opponent, opponent_row + str(j+2))
					eval = (-1) * evaluationFunction(state2, opponent)
					value2.append([opponent_row + str(j+2), eval])	
					#print "here2"
					#print "value2" + str(value2)
					min = findMin(value2)
			value.append([row + str(i+2), evaluationFunction (state1, player)])
			#print "here1"
			#print value
			max = findMax(value)
		
	if player == 1:	
		final_state = move1(state, player, max)
	else: 	
		final_state = move2(state, player, max)
	#print "I reached here"	
	if checkTerminalState(final_state, player) == True:
		stones = 0
		for i in range(0, columns):
			stones += final_state.opponentArray[i]
 			final_state.opponentArray[i] = 0
		final_state.mancala[opponent] += stones
		output_board(final_state)
		return
	else:
		output_board(final_state)
		return

def minimax2 (state, player, cutoff_depth):
	row = "a"
	opponent = 1
	opponent_row = "b"
	while cutoff_depth >= 0:
		cutoff_depth -= 1
		value = []
		for i in range(0, columns):
			if player == 1:
				state1 = move1(state, player, row + str(i+2))
			elif player == 2:
				state1 = move2(state, player, row + str(i+2))
			if cutoff_depth >= 0:
				value2 = []			
				for j in range(0, columns):
					if cutoff_depth >= 0:
						if state1.player == 1:	
							state2 = move2(state1, opponent, opponent_row + str(j+2))
						elif state1.player == 2:
							state2 = move1(state1, opponent, opponent_row + str(j+2))
					eval = (-1) * evaluationFunction(state2, opponent)
					value2.append([opponent_row + str(j+2), eval])	
					#print "here2"
					#print "value2" + str(value2)
					min = findMin(value2)
			value.append([row + str(i+2), evaluationFunction (state1, player)])
			#print "here1"
			#print value
			max = findMax(value)
		
	if player == 1:	
		final_state = move1(state, player, max)
	else: 	
		final_state = move2(state, player, max)
	#print "I reached here"	
	if checkTerminalState(final_state, player) == True:
		stones = 0
		for i in range(0, columns):
			stones += final_state.opponentArray[i]
 			final_state.opponentArray[i] = 0
		final_state.mancala[opponent] += stones
		output_board(final_state)
		return
	else:
		output_board(final_state)
		return
		

def output_board(state):
	if state.player == 1:
		board_state2 = state.opponentArray
		board_state1 = state.playerArray
	else:
		board_state2 = state.playerArray
		board_state1 = state.opponentArray
	for i in range(0, columns):
		value = board_state2[i]
		f1.write(str(value) + str(" "))
	f1.write("\n")
	for i in range(0, columns):
		value = board_state1[i]
		f1.write(str(value) + str(" "))
	f1.write("\n")
	value = str(state.mancala[2])	
	f1.write(value)
	f1.write("\n")
	value = str(state.mancala[1])
	f1.write(value)				
				
	
			
if task == 1:			  
	if player == 1:
		state = State(player, "b2" , playerArray, opponentArray, mancala, 1)	
		#minimax1(state, player, cutoff_depth)
		greedy(state, player)	
	else:
		state = State(player, "a2" , playerArray, opponentArray, mancala, 1)	
		greedy(state, player)
elif task == 2 or task == 3:
	#print "I entered here"
	if player == 1:
		state = State(player, "b2" , playerArray, opponentArray, mancala, 1)	
		minimax1(state, player, cutoff_depth)
	else:
		state = State(player,  "a2" , playerArray, opponentArray, mancala, 1)	
		minimax2(state, player, cutoff_depth)	


        
import copy
import math

f = open('input.txt', 'r')
f1 = open('next_state.txt', 'w')
f2 = open('traverse_log.txt', 'w') 

#f  = open(sys.argv[2],'r')

f1.write("")
task = f.readline().strip('\n');
task = int(task)
max_player = f.readline().strip('\n');
max_player = int(max_player)
print "Player" + str(max_player)
cutoff_depth = f.readline().strip('\n');
cutoff_depth = int(cutoff_depth)
board_state2 = f.readline().strip('\n');
board_state2 = board_state2.split(' ')
board_state1 = f.readline().strip('\n');
board_state1 = board_state1.split(' ')
rows = 2
columns = len(board_state1)

mancala2 = f.readline().strip('\n');
mancala1 = f.readline().strip('\n');


main_board = {}

key = "a1"
value = 0
neighbor = "b2"
main_board[key] = [value, neighbor]
key = "b" + str(columns+2)
value = 0
neighbor = "a" + str(columns+1)
main_board[key] = [value, neighbor]
for i in range(0, columns):
	key = "a" + str(i+2)
	value = int(board_state2[i])
	neighbor = "a" + str(i+1)
	main_board[key] = [value, neighbor]
	key = "b" + str(i+2)
	value = int(board_state1[i])
	neighbor = "b" + str(i+3)
	main_board[key] = [value, neighbor]
print main_board


def check_rules(neighbor, player, board):
	print "in check rules"
	print "player" + str(player)
	print "neighbor" + str(neighbor)
	if (neighbor == "a1" and player ==2):
		print "Condition 1"
		board = evaluation_function(board, 2)
		print board
	elif (neighbor =="b"+str(columns+2) and player == 1):
		print "Condition 2"
		print "rotation_board" + str(board)
		board = evaluation_function(board, 1)
	elif (board[neighbor][0] == 1 and player == 1 and neighbor[0] == "b"):
		print "Condition 3"
		stones_across = board["a"+neighbor[1]][0]
		board["a"+neighbor[1]][0] = 0
		board[neighbor][0] = 0
		board["b"+str(columns+2)][0] = board["b"+str(columns+2)][0] + stones_across + 1
	elif (board[neighbor][0] == 1 and player == 2 and neighbor[0] == "a"):
		print "Condition 4"
		stones_across = board["b"+neighbor[1]][0]
		board["b"+neighbor[1]][0] = 0
		board[neighbor][0] = 0
		board["a1"][0] = board["a1"][0] + stones_across + 1	
	return board	
		

def stone_rotation(rotation_board, source_pit, player):
	print "source pit" + source_pit
	print rotation_board[source_pit][0]
	stones = rotation_board[source_pit][0]
	print "stones " + str(stones)
	if stones == 0:
		return
	rotation_board[source_pit][0] = 0
	#print rotation_board[source_pit][0]
	pit = source_pit
	for i in range(0, stones):
		neighbor = rotation_board[pit][1]
		rotation_board[neighbor][0] += 1
		pit = neighbor
		#print "neighbor" + neighbor
	#print "last neighbor" + neighbor	
	#rotation_board = check_rules(neighbor, player, rotation_board)
	#print "rotation board" + str(rotation_board)
	return rotation_board
	
def node(pit, depth, parent, board):
	row = pit[0]
	if row == "a":
		opponent_row = "b"
		player = 2
	else:
		opponent_row = "a"
		player = 1	
	values = []
	children = []
	print "row" + row
	for i in range(2, (columns+2)):
		children.append(opponent_row + str(i))
	print children
		
	candidate_board = {}
	candidate_board = copy.deepcopy(board)
	candidate_board = stone_rotation(candidate_board, pit, player)	
	values.append([pit, depth, -float('inf'), children, candidate_board])
	print "hello"
	print values		
	return values
		
	
def minimax_decision (board, player, cutoff_depth):
	if player == 1:
		row = "b"
		opponent_row = "a"
	elif player == 2:
		row = "a"
		opponent_row = "b"
	root = ["root", 0, None, board]
	children = []
	for i in range(2, (columns+2)):
		print "board" + str(board)
		print row + str(i)
		value = node(row + str(i), 1, "root", board)
		children.append(value)
	print "children" +str(children)
	minimax(board, children, row, cutoff_depth - 1, True)	
		
def evaluation_minimax (board, pit, player):
	value = []
	print "max_player" + str(max_player)
	print "board" +str(board)
	candidate_board = copy.deepcopy(board)
	candidate_board = stone_rotation(candidate_board, pit, player)
	print "candidate_board" +str(candidate_board)
	mancala1 = candidate_board["b" + str(columns+2)][0]
	print mancala1
	mancala2 = candidate_board["a1"][0]
	print mancala2	
	if max_player == 1:
		evaluation = mancala1 - mancala2
	elif max_player == 2:
		evaluation = mancala2 - mancala1
	value.append([pit, evaluation])
	print "Value minimax" + str(value)
	return value			

def minimax (board, moves, row, depth, maximizing_player):
	if row == "a":
		opponent_row = "b"
		player = 2
	else:
		opponent_row = "a"
		player = 1	
	print "depth" + str(depth)
	if depth == 0:
		print "depth 0 board" + str(board)
		print "moves" +str(moves)
		print moves[0][0][0]
		return evaluation_minimax(board, moves[0][0][0], player)
		
	if maximizing_player:
		best_value = [["", -1000]]
		print moves
		for i in moves:
			print "i" +str(i)
			for j in range(2, (columns+2)):
				children = []
				print "j" +str(j)
				print i
				max_value = node(opponent_row + str(j), i[0][1]+1, i[0][0], i[0][4])
				children.append(max_value)
			#print "children11" + str(children)
				max_value = minimax(i[0][4], children, opponent_row, depth - 1, False)
				print "max_value" + str(max_value)
				print "best_value" + str(best_value[0][1])
				if best_value[0][1] < max_value[0][1]:
					best_value = max_value
					print "best_value after comparing" + str(best_value)
					i[0][2] = best_value[0][1]
					print "Updated i" + str(i)
		return best_value		
	else:
		best_value = [["", 1000]]
		for i in moves:
			children = []
			for j in range(2, (columns+2)):
				min_value = node(row + str(j), i[0][1]+1, i[0][0], i[0][4])
				children.append(min_value)
				min_value = minimax(i[0][4], children, row, depth - 1, True)
				print "min_value" + str(min_value[0][1])
				print "best_value" + str(best_value[0][1])
				if best_value[0][1] > min_value[0][1]:
					best_value = min_value	
		return best_value	
		
			
			
			
print 'here'			
best_value = minimax_decision(main_board, 1, 2)	
print best_value					
